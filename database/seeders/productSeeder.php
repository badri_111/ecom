<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class productSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('products')->insert([
         
            [
                'name'=>' LG mobile',
                 'price'=>'5000',
                 'category'=>'mobile',   
                 'description'=>'mobile with 4gb ram and 64gb hard disk',
                 'gallery'=>'images/lgphone.jfif'       
                
            ],
            [
                'name'=>' Lg fridge',
                 'price'=>'5000',
                 'category'=>'fridge',   
                 'description'=>'fridge with low power capacity',
                 'gallery'=>'images/fridgelg.jfif'       
                
            ]
        ]);
    }
}
